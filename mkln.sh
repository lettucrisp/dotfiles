#!/bin/bash

DIR=$(cd $(dirname $0); pwd)

# EditorConfig
ln -sf $DIR/.editorconfig ~/.editorconfig

# Vim
mkdir -p ~/.vim/rc 2>/dev/null
ln -sf $DIR/.vimrc ~/.vimrc
ln -sf $DIR/.gvimrc ~/.gvimrc
ln -sf $DIR/dein_plugins.toml ~/.vim/rc/dein_plugins.toml
ln -sf $DIR/vimtemplate ~/.vim/template

# Latex
ln -sf $DIR/.latexmkrc ~/.latexmkrc

# Bash
ln -sf $DIR/.bashrc ~/.bashrc

# Zsh
ln -sf $DIR/.zshrc ~/.zshrc

# each os process
if [ "$(uname)" == 'Darwin' ]; then
  echo "MacOS"
  while true; do
    read -n 1 -p 'Install homebrew? [y/n]' yn
    echo ""
    case $yn in
      [Yy] )
          /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
          break;;
      [Nn] ) break;;
      * ) echo "type y/n";;
    esac
  done

  mkdir -p ~/.config/karabiner/assets/complex_modifications
  ln -sf $DIR/karabiner/* ~/.config/karabiner/assets/complex_modifications/
elif [ "$(expr substr $(uname -s) 1 5)" == 'Linux' ]; then
  echo "Linux"
elif [ "$(expr substr $(uname -s) 1 10)" == 'MINGW32_NT' ]; then
  echo "MinGW"
else
  echo "OS??"
fi

# install zsh zplug
while true; do
    read -n 1 -p 'Install zplug? [y/n]' yn
    echo ""
    case $yn in
        [Yy] )
            curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh
            break;;
        [Nn] ) break;;
        * ) echo "type y/n";;
    esac
done

# install gibo
while true; do
    read -n 1 -p 'Install gibo? [y/n]' yn
    echo ""
    case $yn in
        [Yy] )
            git clone https://github.com/simonwhitaker/gibo.git ~/.gibo
            break;;
        [Nn] ) break;;
        * ) echo "type y/n";;
    esac
done

