
set encoding=utf-8
scriptencoding utf-8

"======================================
" Vim setting file
"   Date   : 2016/07/06
"======================================

"======================================
" Basic settings
"======================================

" Leader
noremap <Space> <Nop>
let g:mapleader = "\<Space>"

" Not vi mode
if &compatible
  set nocompatible
endif

" Sync OS Clipboard
set clipboard+=unnamed

set confirm     " Confirm when closing not saved
set nobackup    " Disable backup file
set noundofile  " Disable undo session file

" File encoding and eol type
set fileencoding=utf-8
set fileencodings=iso-2022-jp,euc-jp,sjis,utf-8
set fileformats=unix,dos,mac

set number      " Line number
set ruler       " Ruler
set list        " Show tab or eol
set listchars=tab:>-,extends:<,trail:-,eol:<
set cursorline  " Show cursor line
set tabstop=4   " Tab width
set autoindent  " Auto indent
set smartindent " Smart indent
set expandtab   " Use soft tab
set shiftwidth=4  " Use 4 space as tab
set softtabstop=4 " Soft tab width is 4
set colorcolumn=80 " 80 charactors line
set backspace=indent,eol,start " Use BS to delete line
set formatoptions+=mM

set hlsearch    " Highlight search results
set incsearch   " Enable incrimental search
set ignorecase  " Ignore case
set smartcase   " If search string is starting with upper case, not ignorecase
set wrapscan    " Search looping
set wrap        " Wrap text overflow
set breakindent " Indent wraped text
set virtualedit=block " Enable virtual line in virtual block
set scrolloff=8 " Scroll margin
set showcmd

set wildmenu    "
set showmatch   " Show pair brace
set matchpairs& matchpairs+=<:>

set hidden      " Allow multiple edit
set autoread    " Auto reload when editing file
set switchbuf=useopen " Use buffer already opened

set mouse=a " Enable mouse interaction
set visualbell t_vb=  " Disable bells
set noerrorbells
set laststatus=2

" color mode
if !has('gui_running')
  set t_Co=256
endif

" Hide menu and toolbar
set guioptions-=m
set guioptions-=T

" Template
autocmd BufNewFile *.py 0r $HOME/.vim/template/python.txt
autocmd BufNewFile *.tex 0r $HOME/.vim/template/latex.txt

autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" User defined variables
let userhome=expand('~')
let vimconfigdir=userhome . '/.vim'

"======================================
" Dein install and load
"======================================
let s:dein_dir = expand('~/.vim/dein')
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

if &runtimepath !~# 'dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . s:dein_repo_dir
endif

"======================================
" Dein plugins install and load
"======================================
if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)

  " TOML file of plugins
  let g:rc_dir = expand('~/.vim/rc')
  let s:p_toml = g:rc_dir . '/dein_plugins.toml'

  " load TOML and cache it
  call dein#load_toml(s:p_toml, {'lazy': 0})

  call dein#end()
  call dein#save_state()
endif

if dein#check_install()
  call dein#install()
endif

"======================================
" Plugin settings
"======================================

" unite settings
let g:unite_enable_start_insert=1

" neocomplete
let g:neocomplete#enable_at_startup               = 1
let g:neocomplete#auto_completion_start_length    = 3
let g:neocomplete#enable_ignore_case              = 1
let g:neocomplete#enable_smart_case               = 1
let g:neocomplete#enable_camel_case               = 1
let g:neocomplete#use_vimproc                     = 1
let g:neocomplete#sources#buffer#cache_limit_size = 1000000
let g:neocomplete#sources#tags#cache_limit_size   = 30000000
let g:neocomplete#enable_fuzzy_completion         = 1
let g:neocomplete#lock_buffer_name_pattern        = '\*ku\*'

" vimtex hokan for neocomplete
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
let g:neocomplete#sources#omni#input_patterns.tex =
  \ '\v\\%('
  \ . '\a*%(ref|cite)\a*%(\s*\[[^]]*\])?\s*\{[^{}]*'
  \ . '|includegraphics%(\s*\[[^]]*\])?\s*\{[^{}]*'
  \ . '|%(include|input)\s*\{[^{}]*'
  \ . ')'

if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
let g:neocomplete#sources#omni#input_patterns.tex =
  \ '\v\\%('
  \ . '\a*cite\a*%(\s*\[[^]]*\]){0,2}\s*\{[^}]*'
  \ . '|\a*ref%(\s*\{[^}]*|range\s*\{[^,}]*%(}\{)?)'
  \ . '|hyperref\s*\[[^]]*'
  \ . '|includegraphics\*?%(\s*\[[^]]*\]){0,2}\s*\{[^}]*'
  \ . '|%(include%(only)?|input)\s*\{[^}]*'
  \ . '|\a*(gls|Gls|GLS)(pl)?\a*%(\s*\[[^]]*\]){0,2}\s*\{[^}]*'
  \ . '|includepdf%(\s*\[[^]]*\])?\s*\{[^}]*'
  \ . '|includestandalone%(\s*\[[^]]*\])?\s*\{[^}]*'
  \ . ')'

" vim-clang settings

" default 'longest' can not work with neocomplete
let g:clang_c_completeopt   = 'menuone'
let g:clang_cpp_completeopt = 'menuone'

if executable('clang++-3.6')
    let g:clang_exec = 'clang++-3.6'
elseif executable('clang++-3.5')
    let g:clang_exec = 'clang++-3.5'
elseif executable('clang++-3.4')
    let g:clang_exec = 'clang++-3.4'
else
    let g:clang_exec = 'clang++'
endif

if executable('clang-format-3.6')
    let g:clang_format_exec = 'clang-format-3.6'
elseif executable('clang-format-3.5')
    let g:clang_format_exec = 'clang-format-3.5'
elseif executable('clang-format-3.4')
    let g:clang_format_exec = 'clang-format-3.4'
else
    let g:clang_exec = 'clang-format'
endif

let g:clang_cpp_options = '-std=c++11 -stdlib=libc++'

" vimfiler settings
let g:vimfiler_as_default_explorer  = 1
let g:vimfiler_safe_mode_by_default = 0
let g:vimfiler_data_directory       = expand('~/.vim/etc/vimfiler')

" vim-quickrun settings
let g:quickrun_config = {
  \ '_' : {
  \   'runner' : 'vimproc',
  \   'runner/vimproc/updatetime' : 40,
  \   'outputter' : 'error',
  \   'outputter/buffer/into' : 1,
  \   'outputter/error/success' : 'buffer',
  \   'outputter/error/error'   : 'quickfix',
  \   'outputter/buffer/split'  : ':botright 8sp',
  \   'outputter/buffer/close_on_empty' : 1,
  \   'hook/shabadoubi_touch_henshin/enable' : 1,
  \   'hook/shabadoubi_touch_henshin/wait' : 40,
  \   'hook/close_quickfix/enable_hook_loaded' : 1,
  \   'hook/close_quickfix/enable_success' : 1,
  \ }}

" quickrun for tex
let g:quickrun_config['tex'] = {
  \ 'command' : 'latexmk',
  \ 'outputter' : 'error',
  \ 'outputter/error/success' : 'null',
  \ 'outputter/error/error' : 'quickfix',
  \ 'srcfile' : expand("%"),
  \ 'cmdopt': '-pdfdvi -pv',
  \ 'hook/sweep/files' : [
  \                      '%S:p:r.aux',
  \                      '%S:p:r.bbl',
  \                      '%S:p:r.blg',
  \                      '%S:p:r.dvi',
  \                      '%S:p:r.fdb_latexmk',
  \                      '%S:p:r.fls',
  \                      '%S:p:r.log',
  \                      '%S:p:r.out'
  \                      ],
  \ 'exec': '%c %o %a %s',
  \ }

" lightline settings
let g:lightline = {
  \ 'colorscheme' : 'jellybeans',
  \ }

"======================================
" Additional Setting
"======================================

filetype plugin indent on
syntax on " Use syntax highlight

" Color scheme setting
colorscheme lucius
LuciusDark

" Use these keys to move above line
set whichwrap=b,s,h,l,<,>,[,],~

"======================================
" Key binds
"======================================

" コロンとセミコロンを入れ替え
noremap ; :
noremap : ;

" 分割関連のマッピング
" Horizontal split
nnoremap <silent> sr :<C-u>sp<CR>
" Vertical split
nnoremap <silent> sc :<C-u>vs<CR>
" Activate upper window
nnoremap <silent> sw <C-w>k
" Activate bottom window
nnoremap <silent> ss <C-w>j
" Activaete right window
nnoremap <silent> sd <C-w>l
" Activate left window
nnoremap <silent> sa <C-w>h
" Close current window
nnoremap <silent> sq :<C-u>q<CR>

" タブ関連のマッピンg
" Open new tab
nnoremap <silent> \ :<C-u>tabnew<CR>
" Activate next tab
nnoremap <silent> = gt
" Activate previous tab
nnoremap <silent> - gT
" Close current tab
nnoremap <silent> \| :<C-u>tabclose<CR>

" Move wrapped lines directly
nnoremap <silent> j gj
nnoremap <silent> k gk
nnoremap <silent> gj j
nnoremap <silent> gk k
nnoremap <silent> <Up> gk
nnoremap <silent> <Down> gj

" <C-c> to <Esc>
noremap <silent> <C-c> <Esc>
noremap! <silent> <C-c> <Esc>
inoremap <silent> jj <Esc>

" Centerize found string
nnoremap <silent> n nzz
nnoremap <silent> N Nzz
nnoremap <silent> * *zz
nnoremap <silent> # #zz
nnoremap <silent> g* g*zz
nnoremap <silent> g# g#zz

" Jump to pair by TAB
nnoremap <silent> <Tab> %
vnoremap <silent> <Tab> %

" Select line exclude linebreak
noremap <silent> <Leader>v 0v$h
" Select line after cursor
noremap <silent> <Leader>V v$h
" Copy line after cursor
noremap <silent> <Leader>y v$h"*y

" Select all
"noremap <C-a> ggvG$
"noremap! <C-a> <Esc>ggvG$

" Enable BS in normal mode
nnoremap <silent> <Bs> "_X
" Enable BS in virtual mode
vnoremap <silent> <Bs> "_x

" Close help by q
autocmd FileType help nnoremap <silent> q :q<CR>

"======================================
" Plugin key binds
"======================================

" unite key binds
nnoremap <silent> <Leader>b :<C-u>Unite buffer -no-start-insert<CR>
nnoremap <silent> <Leader>uf :<C-u>UniteWithBufferDir -buffer-name=files file<CR>
nnoremap <silent> <Leader>hp :<C-u>Unite -buffer-name=register register<CR>
nnoremap <silent> <Leader>hf :<C-u>Unite file_mru<CR>
nnoremap <silent> <Leader>hb :<C-u>Unite buffer file_mru<CR>
nnoremap <silent> <Leader>ha :<C-u>UniteWithBufferDir -buffer-name=files buffer file_mru bookmark file<CR>
autocmd FileType unite nmap <silent> <buffer> <expr> <C-j> unite#do_action('split')
autocmd FileType Unite imap <silent> <buffer> <expr> <C-j> unite#do_action('split')
autocmd FileType unite nmap <silent> <buffer> <expr> <C-l> unite#do_action('vsplit')
autocmd FileType unite imap <silent> <buffer> <expr> <C-l> unite#do_action('vsplit')
autocmd FileType unite nmap <silent> <buffer> <ESC><ESC> q
autocmd FileType unite imap <silent> <buffer> <ESC><ESC> <ESC>q
autocmd FileType unite nmap <silent> <buffer> <C-c><C-c> q
autocmd FileType unite imap <silent> <buffer> <C-c><C-c> <Esc>q

" vimshell key binds
nnoremap <silent> vs :<C-u>VimShell<CR>
nnoremap <silent> vp :<C-u>VimShellPop<CR>

" vimfiler key binds
nnoremap sf :<C-u>VimFiler<CR>
nnoremap <silent><Leader>f :<C-u>VimFilerBufferDir -split -simple -winwidth=35 -no-quit -toggle<CR>

" unite-outline key binds
let g:unite_split_rule = 'botright'
nnoremap <silent><Leader>o :<C-u>Unite -vertical -winwidth=30 -no-quit outline<CR>

" QuickRun key binds
nnoremap <silent><Leader>e :<C-u>QuickRun<CR>
autocmd FileType qf nnoremap <silent><buffer>q :quit<CR>

" <Leader><C-e> To change quickrun buffer argument
nnoremap <Leader><C-e> :<C-u>let b:quickrun_config={'args': ''}



